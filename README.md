## Trello clone
This application is a clone of Trello Kanban-like lists and card management application.
It includes:
- Custom Server Side Rendering (currently turned off beacouse of data handling with localStorage)
- Lazy loading and code splitting 🚀
- React/Redux
- Test coverage ~90% (with jest and react-testing-library)
- Drag and drop implemented with react-beautifull-dnd

# Requirements
- [x] List creation
- [x] List name edition (with validation of non-empty <= 30 length string)
- [x] Task creation
- [x] Supported persistence (localStorage)
- [x] Support multiple boards
- [x] Support app routing for boards
- [x] Suport drag and drop for cards
- [x] Support drag and drop for a list

# Installation
```
    npm install
    npm start
```

# Tests
To run test use one of te following commands:
```
    npm run test
    npm run test:coverage
```

File                                               |  % Stmts | % Branch |  % Funcs |  % Lines |
---------------------------------------------------|----------|----------|----------|----------|
All files                                          |    97.52 |    77.94 |    89.91 |    99.21 |

# Storybooks
To run storybooks use following command:
```
    npm run storybook
```

![](managing_lists.gif)
![](moving_lists.gif)