const path = require('path')
const webpack = require('webpack')
const nodeExternals = require('webpack-node-externals')

module.exports = {
  target: 'node',
  name: 'server',
  mode: 'production',
  entry: ['babel-polyfill', path.resolve(__dirname, '../src/server.js')],
  output: {
    filename: 'server.js',
    path: path.resolve(__dirname, '../dist/server'),
    publicPath: '/',
    libraryTarget: 'commonjs2',
  },
  externals: [
    nodeExternals(),
  ],
  module: {
    rules: [
      // { enforce: 'pre', test: /\.jsx?$/, loader: 'eslint-loader', exclude: /node_modules/ },
      { test: /\.jsx?$/, exclude: /node_modules/, use: 'babel-loader' },
      { test: /\.svg$/, loader: 'react-svg-loader' },
    ],
  },
  devtool: false,
  plugins: [
    new webpack.optimize.LimitChunkCountPlugin({
      maxChunks: 1,
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production'),
      },
    }),
  ],
}
