const path = require('path')
const webpack = require('webpack')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const { ReactLoadablePlugin } = require('react-loadable/webpack')

module.exports = {
  entry: ['babel-polyfill', path.resolve(__dirname, '../src/client.js')],
  name: 'client',
  target: 'web',
  mode: 'production',
  output: {
    filename: 'static/[name].[chunkhash].js',
    chunkFilename: 'static/[name].[chunkhash].js',
    path: path.resolve(__dirname, '../public'),
    publicPath: '/',
  },
  module: {
    rules: [
      // { enforce: 'pre', test: /\.jsx?$/, loader: 'eslint-loader', exclude: /node_modules/ },
      { test: /\.jsx?$/, exclude: /node_modules/, use: 'babel-loader' },
      { test: /\.svg$/, loader: 'react-svg-loader' },
    ],
  },
  devtool: false,
  plugins: [
    new CleanWebpackPlugin(['public'], { root: path.resolve(__dirname, '..'), allowExternal: true, watch: true }),
    new ReactLoadablePlugin({
      filename: path.resolve(__dirname, '../dist', 'react-loadable.json'),
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production'),
      },
    }),
  ],
}
