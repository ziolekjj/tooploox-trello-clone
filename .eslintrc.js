module.exports = {
    "plugins": ["jest"],
    "extends": ["plugin:react/recommended", "standard"],
    "parser": "babel-eslint",
    "parserOptions": {
      "sourceType": "module",
      "allowImportExportEverywhere": false,
      "codeFrame": false
    },
    "rules": {
        "comma-dangle": ["error", "always-multiline"]
    },
    "env": {
        "jest/globals": true
    }
};
