import express from 'express'
import Loadable from 'react-loadable'
import initStore from './store'
import template from './template'

const app = express()

app.use(express.static('public'))
app.get('*', (req, res) => {
  const store = initStore()
  const context = {}
  const content = template(req, store, context)

  if (context.url) {
    res.redirect(301, context.url)
  } else {
    res.send(content)
  }
})

Loadable.preloadAll().then(() => {
  app.listen(3000, () => {
    console.log('Running on http://localhost:3000/')
  })
})
