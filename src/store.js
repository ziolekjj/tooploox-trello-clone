import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { set } from './utils/localStorage'
import reducers from './reducers'

const _initialState = {
  boards: { my_board: { id: 'my_board', boardName: 'My board', lists: [] } },
  lists: {},
  tasks: {},
}

const initStore = ({ initialState = _initialState } = {}) => {
  const store = createStore(reducers, initialState, applyMiddleware(thunk))
  store.subscribe(() => {
    set('__persistedState', store.getState())
  })
  return store
}

export default initStore
