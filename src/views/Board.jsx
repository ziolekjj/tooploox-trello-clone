import React from 'react'
import { arrayOf, string, shape, func } from 'prop-types'
import { connect } from 'react-redux'
import { Redirect } from 'react-router'
import Loadable from 'react-loadable'
import { DragDropContext, Droppable } from 'react-beautiful-dnd'
import styled from 'styled-components'
import { moveList } from '../actions/boards'
import { moveTask } from '../actions/lists'
import ListInput from '../components/ListInput'

const List = Loadable({
  loader: () => import('../components/List'),
  loading: () => null,
})

class Board extends React.Component {
  onDragEnd = ({ destination, source, draggableId, type }) => {
    if (!destination) return
    const {
      moveTask,
      moveList,
      match: {
        params: { id },
      },
    } = this.props
    if (type === 'list') {
      moveList({
        boardId: id,
        sourceIndex: source.index,
        destinationIndex: destination.index,
      })
    } else {
      moveTask({
        taskId: draggableId,
        sourceListId: source.droppableId,
        destinationListId: destination.droppableId,
        sourceIndex: source.index,
        destinationIndex: destination.index,
      })
    }
  }

  render () {
    const { lists, match } = this.props
    if (!lists) return <Redirect from="/boards/:id" to="/" />
    return (
      <Container>
        <DragDropContext onDragEnd={this.onDragEnd}>
          <Droppable droppableId="board" direction="horizontal" type="list">
            {provided => (
              <ListArea {...provided.droppableProps} innerRef={provided.innerRef}>
                {lists.map((id, index) => (
                  <List key={id} id={id} index={index} />
                ))}
                {provided.placeholder}
              </ListArea>
            )}
          </Droppable>
        </DragDropContext>
        <InputSectionWrapper>
          <ListInput boardId={match.params.id}/>
        </InputSectionWrapper>
      </Container>
    )
  }
}

Board.propTypes = {
  match: shape({
    params: shape({
      id: string,
    }).isRequired,
  }).isRequired,
  lists: arrayOf(string),
  moveTask: func.isRequired,
}

const Container = styled.div`
  font-family: 'Helvetica Neue', Arial, Helvetica, sans-serif;
  display: flex;
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  width: 100%;
  overflow-x: scroll;
  background-color: #0079bf;
`

const ListArea = styled.section`
  display: flex;
`

const mapStateToProps = (state, props) => {
  const { match: { params: { id } } } = props
  return ({
    lists: (state.boards[id] && state.boards[id].lists) || null,
  })
}

const mapDispatchToProps = {
  moveList,
  moveTask,
}

const InputSectionWrapper = styled.div`
  margin: 5px;
  min-width: 290px;
  min-height: 40px;
`

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Board)
