import React from 'react'
import { string, arrayOf, shape } from 'prop-types'
import styled from 'styled-components'
import { connect } from 'react-redux'
import media from '../utils/media'
import BoardEntry from '../components/BoardEntry'
import BoardInput from '../components/BoardInput'

class HomePage extends React.Component {
  render () {
    const { boards } = this.props
    return (
      <Container>
        <MainColumn>
          {boards.map(({ id, boardName }) => (
            <BoardEntry key={id} id={id}>
              {boardName}
            </BoardEntry>
          ))}
          <InputWrapper>
            <BoardInput />
          </InputWrapper>
        </MainColumn>
      </Container>
    )
  }
}

HomePage.propTypes = {
  boards: arrayOf(shape({
    id: string,
    boardName: string,
  })).isRequired,
}

const Container = styled.div`
  background-color: white;
  width: 100%;
  height: 100%;
  position: absolute;
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  font-family: 'Helvetica Neue', Arial, Helvetica, sans-serif;
`

const MainColumn = styled.div`
  width: 70%;
  display: flex;
  align-content: flex-start;
  flex-wrap: wrap;
  ${media.third`
    width: 100%;
  `};
`

const InputWrapper = styled.div`
  margin: 5px;
  width: calc(25% - 10px);
  ${media.third`
    width: calc(33% - 10px);
  `};
  ${media.half`
    width: calc(50% - 10px);
  `};
`

const mapStateToProps = state => {
  const boards = Object.keys(state.boards).map(id => state.boards[id])
  return ({
    boards,
  })
}

export default connect(mapStateToProps)(HomePage)
