import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { renderRoutes } from 'react-router-config'
import { Provider } from 'react-redux'
import Loadable from 'react-loadable'
import { get } from './utils/localStorage'
import routes from './routes'

import initStore from './store'

const persistedState = get('__persistedState') || {}
const initialState = {
  ...window.INITIAL_STATE,
  ...persistedState,
}

const store = initStore({ initialState })

Loadable.preloadReady().then(() => {
  ReactDOM.render(
    <Provider store={store}>
      <BrowserRouter>
        <div>{renderRoutes(routes)}</div>
      </BrowserRouter>
    </Provider>,
    document.getElementById('root'),
  )
})
