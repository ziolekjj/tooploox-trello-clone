import { combineReducers } from 'redux'
import boardsReducer from './boards'
import listsReducer from './lists'
import tasksReducer from './tasks'

const reducers = combineReducers({
  boards: boardsReducer,
  lists: listsReducer,
  tasks: tasksReducer,
})

export default reducers
