import { CREATE_BOARD, EDIT_BOARD, ADD_LIST_TO_BOARD, MOVE_LIST } from '../constants/actionTypes'
import { moveElement } from '../utils/move'

const listsReducer = (state = {}, action) => {
  switch (action.type) {
    case CREATE_BOARD:
      return {
        ...state,
        [action.data.id]: { ...action.data, lists: [] },
      }
    case EDIT_BOARD:
      return {
        ...state,
        [action.data.id]: {
          ...state[action.data.id],
          ...action.data,
        },
      }
    case ADD_LIST_TO_BOARD:
      return {
        ...state,
        [action.data.boardId]: {
          ...state[action.data.boardId],
          lists: [...state[action.data.boardId].lists, action.data.listId],
        },
      }
    case MOVE_LIST:
      const array = state[action.data.boardId].lists
      const { sourceIndex, destinationIndex } = action.data
      const newList = moveElement({ array, sourceIndex, destinationIndex })
      return {
        ...state,
        [action.data.boardId]: {
          ...state[action.data.boardId],
          lists: newList,
        },
      }
    default:
      return state
  }
}

export default listsReducer
