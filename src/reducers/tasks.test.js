import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { CREATE_TASK, EDIT_TASK } from '../constants/actionTypes'
import tasksReducer from './tasks'

const mockStore = configureStore([thunk])

test('tasksReducer - CREATE_TASK', () => {
  const store = mockStore({})

  const action = {
    type: CREATE_TASK,
    data: { id: '1', description: 'Do something' },
  }

  const expectedState = {
    '1': {
      id: '1',
      description: 'Do something',
    },
  }

  expect(tasksReducer(store.getState(), action)).toEqual(expectedState)
})

test('tasksReducer - EDIT_TASK', () => {
  const initialState = {
    '1': {
      id: '1',
      description: 'Do something',
    },
  }

  const store = mockStore(initialState)

  const action = {
    type: EDIT_TASK,
    data: { id: '1', description: 'Edited something' },
  }

  const expectedState = {
    '1': {
      id: '1',
      description: 'Edited something',
    },
  }

  expect(tasksReducer(store.getState(), action)).toEqual(expectedState)
})
