import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { CREATE_BOARD, EDIT_BOARD, ADD_LIST_TO_BOARD, MOVE_LIST } from '../constants/actionTypes'
import boardsReducer from './boards'

const mockStore = configureStore([thunk])

test('boardsReducer - CREATE_BOARD', () => {
  const store = mockStore({})

  const action = {
    type: CREATE_BOARD,
    data: { id: '1', boardName: 'New board' },
  }

  const expectedState = {
    '1': {
      id: '1',
      boardName: 'New board',
      lists: [],
    },
  }

  expect(boardsReducer(store.getState(), action)).toEqual(expectedState)
})

test('boardsReducer - EDIT_BOARD', () => {
  const initialState = {
    '1': {
      id: '1',
      boardName: 'New board',
      lists: [],
    },
  }

  const store = mockStore(initialState)

  const action = {
    type: EDIT_BOARD,
    data: { id: '1', boardName: 'Edited board' },
  }

  const expectedState = {
    '1': {
      id: '1',
      boardName: 'Edited board',
      lists: [],
    },
  }

  expect(boardsReducer(store.getState(), action)).toEqual(expectedState)
})

test('boardsReducer - ADD_LIST_TO_BOARD', () => {
  const initialState = {
    '1': {
      id: '1',
      boardName: 'New board',
      lists: [],
    },
  }

  const store = mockStore(initialState)

  const action = {
    type: ADD_LIST_TO_BOARD,
    data: { listId: 'aaa', boardId: '1' },
  }

  const expectedState = {
    '1': {
      id: '1',
      boardName: 'New board',
      lists: ['aaa'],
    },
  }

  expect(boardsReducer(store.getState(), action)).toEqual(expectedState)
})

test('boardsReducer - MOVE_LIST', () => {
  const initialState = {
    '1': {
      id: '1',
      boardName: 'New board',
      lists: [1, 2],
    },
  }

  const store = mockStore(initialState)

  const action = {
    type: MOVE_LIST,
    data: { boardId: '1', sourceIndex: 0, destinationIndex: 1 },
  }

  const expectedState = {
    '1': {
      id: '1',
      boardName: 'New board',
      lists: [2, 1],
    },
  }

  expect(boardsReducer(store.getState(), action)).toEqual(expectedState)
})
