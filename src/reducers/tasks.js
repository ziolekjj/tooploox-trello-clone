import { CREATE_TASK, EDIT_TASK } from '../constants/actionTypes'

const tasksReducer = (state = {}, action) => {
  switch (action.type) {
    case CREATE_TASK:
      return ({ ...state, [action.data.id]: { ...action.data } })
    case EDIT_TASK:
      return ({ ...state, [action.data.id]: { ...state[action.data.id], ...action.data } })
    default:
      return state
  }
}

export default tasksReducer
