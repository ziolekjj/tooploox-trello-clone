import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { CREATE_LIST, EDIT_LIST, ADD_TASK_TO_LIST, MOVE_TASK } from '../constants/actionTypes'
import listsReducer from './lists'

const mockStore = configureStore([thunk])
const initialState = {
  '1': {
    id: '1',
    listName: 'listName',
    tasks: [0, 1, 2],
  },
}

test('listsReducer - CREATE_LIST', () => {
  const store = mockStore({})

  const action = {
    type: CREATE_LIST,
    data: { id: '1', listName: 'listName' },
  }

  const expectedState = {
    '1': {
      id: '1',
      listName: 'listName',
      tasks: [],
    },
  }

  expect(listsReducer(store.getState(), action)).toEqual(expectedState)
})

test('listsReducer - EDIT_LIST', () => {
  const store = mockStore(initialState)

  const action = {
    type: EDIT_LIST,
    data: { id: '1', listName: 'editedName' },
  }

  const expectedState = {
    '1': {
      id: '1',
      listName: 'editedName',
      tasks: [0, 1, 2],
    },
  }

  expect(listsReducer(store.getState(), action)).toEqual(expectedState)
})

test('listsReducer - ADD_TASK_TO_LIST', () => {
  const store = mockStore(initialState)

  const action = {
    type: ADD_TASK_TO_LIST,
    data: { listId: '1', taskId: 3 },
  }

  const expectedState = {
    '1': {
      id: '1',
      listName: 'listName',
      tasks: [0, 1, 2, 3],
    },
  }

  expect(listsReducer(store.getState(), action)).toEqual(expectedState)
})

test('listsReducer - MOVE_TASK (between different lists)', () => {
  const store = mockStore({
    ...initialState,
    '2': {
      id: '2',
      listName: 'Another list',
      tasks: ['a', 'b'],
    },
  })

  const action = {
    type: MOVE_TASK,
    data: { sourceListId: '1', destinationListId: '2', sourceIndex: 0, destinationIndex: 2 },
  }

  const expectedState = {
    '1': {
      id: '1',
      listName: 'listName',
      tasks: [1, 2],
    },
    '2': {
      id: '2',
      listName: 'Another list',
      tasks: ['a', 'b', 0],
    },
  }

  expect(listsReducer(store.getState(), action)).toEqual(expectedState)
})

test('listsReducer - MOVE_TASK (same list)', () => {
  const store = mockStore(initialState)

  const action = {
    type: MOVE_TASK,
    data: { sourceListId: '1', destinationListId: '1', sourceIndex: 0, destinationIndex: 2 },
  }

  const expectedState = {
    '1': {
      id: '1',
      listName: 'listName',
      tasks: [1, 2, 0],
    },
  }

  expect(listsReducer(store.getState(), action)).toEqual(expectedState)
})
