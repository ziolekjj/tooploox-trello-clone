import { CREATE_LIST, EDIT_LIST, ADD_TASK_TO_LIST, MOVE_TASK } from '../constants/actionTypes'
import { moveElement, moveElementBetweenArrays } from '../utils/move'

const listsReducer = (state = {}, action) => {
  switch (action.type) {
    case CREATE_LIST:
      return {
        ...state,
        [action.data.id]: { ...action.data, tasks: [] },
      }
    case EDIT_LIST:
      return {
        ...state,
        [action.data.id]: {
          ...state[action.data.id],
          ...action.data,
        },
      }
    case ADD_TASK_TO_LIST:
      return {
        ...state,
        [action.data.listId]: {
          ...state[action.data.listId],
          tasks: [...state[action.data.listId].tasks, action.data.taskId],
        },
      }
    case MOVE_TASK:
      if (action.data.sourceListId !== action.data.destinationListId) {
        const sourceArray = state[action.data.sourceListId].tasks
        const destinationArray = state[action.data.destinationListId].tasks
        const { sourceIndex, destinationIndex } = action.data
        const { newSourceArray, newDestinationArray } = moveElementBetweenArrays({
          sourceArray,
          destinationArray,
          sourceIndex,
          destinationIndex,
        })
        return {
          ...state,
          [action.data.sourceListId]: {
            ...state[action.data.sourceListId],
            tasks: newSourceArray,
          },
          [action.data.destinationListId]: {
            ...state[action.data.destinationListId],
            tasks: newDestinationArray,
          },
        }
      } else {
        const array = state[action.data.sourceListId].tasks
        const { sourceIndex, destinationIndex } = action.data
        const newList = moveElement({ array, sourceIndex, destinationIndex })
        return {
          ...state,
          [action.data.sourceListId]: {
            ...state[action.data.sourceListId],
            tasks: newList,
          },
        }
      }
    default:
      return state
  }
}

export default listsReducer
