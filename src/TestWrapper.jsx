import React from 'react'
import { Provider } from 'react-redux'
import { render } from 'react-testing-library'
import initStore from './store'

const renderWrapper = (
  component,
  { initialState, store = initStore({ initialState }) } = {},
) => ({
  ...render(
    <Provider store={store}>
      {component}
    </Provider>
  ),
  store,
})

export { renderWrapper as render }
