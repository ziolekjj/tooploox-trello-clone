// import React from 'react'
// import { renderToString } from 'react-dom/server'
// import { StaticRouter } from 'react-router-dom'
// import { renderRoutes } from 'react-router-config'
// import Loadable from 'react-loadable'
// import { Provider } from 'react-redux'
import { resetServerContext } from 'react-beautiful-dnd'
import { getBundles } from 'react-loadable/webpack'
import { ServerStyleSheet } from 'styled-components'
import serialize from 'serialize-javascript'
// import routes from './routes'

const stats = require('../dist/react-loadable.json')

export default (req, store, context) => {
  const modules = []
  const sheet = new ServerStyleSheet()
  resetServerContext()

  // SSR currently disabled beacouse of using localStorage instead of some database
  // to store data...

  // const content = renderToString(
  //   sheet.collectStyles(
  //     <Loadable.Capture report={moduleName => modules.push(moduleName)}>
  //       <Provider store={store}>
  //         <StaticRouter location={req.url} context={context}>
  //           <div>{renderRoutes(routes)}</div>
  //         </StaticRouter>
  //       </Provider>
  //     </Loadable.Capture>,
  //   ),
  // )

  const style = sheet.getStyleTags()
  const bundles = getBundles(stats, modules)
  const scripts = bundles.filter(bundle => bundle.file.endsWith('.js'))

  return `
    <html>
      <head>
        <title>jziolkowski</title>
        ${style}
      </head>
      <body>
        <div id="root"></div>
        <script>
          window.INITIAL_STATE = ${serialize(store.getState())}
        </script>
        ${scripts.map(script => `<script src="${script.publicPath}" defer></script>`)}
        <script src="/main.js" defer></script>
      </body>
    </html>
  `
}
