import React from 'react'
import { storiesOf } from '@storybook/react'
import SubmitButton from '../SubmitButton'

storiesOf('Submit button', module)
  .add('Standard Submit Button', () => (
    <SubmitButton>Text</SubmitButton>
  ))
