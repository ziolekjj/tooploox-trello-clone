import styled from 'styled-components'

const Button = styled.button`
  background-color: rgb(90, 170, 70);
  box-shadow: 0 1px 0 0 rgb(60, 120, 40);
  border-radius: 3px;
  border: none;
  height: 30px;
  width: 60px;
  margin-right: 5px;
  color: white;
  cursor: pointer;
  font-size: inherit;

  &:hover {
    background-color: rgb(70, 150, 40);
  }

  &:active {
    background-color: rgb(60, 120, 40);
  }

  &:focus {
    outline: none;
  }
`

export default Button
