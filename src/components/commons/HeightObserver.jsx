import React from 'react'
import { func, number } from 'prop-types'

export default class HeightObserver extends React.Component {
  state = {
    height: this.props.initialHeight,
  }
  _ref = React.createRef()

  componentDidMount () {
    const { scrollHeight: height } = this._ref.current
    this.setState({ height })
  }

  componentDidUpdate (_, prevState) {
    const { scrollHeight: height } = this._ref.current
    if (height !== prevState.height) { this.setState({ height }) }
  }

  render () {
    const { height } = this.state
    const { children } = this.props
    return (
      children({ height, ref: this._ref })
    )
  }
}

HeightObserver.propTypes = {
  children: func.isRequired,
  initialHeight: number,
}

HeightObserver.defaultProps = {
  initialHeight: 0,
}
