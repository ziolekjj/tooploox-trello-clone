import React from 'react'
import { func, string } from 'prop-types'

export default class BluredInput extends React.Component {
  initialState = { active: false, input: this.props.initialValue }

  state = this.initialState

  _timeout = null

  static stateChangeTypes = {
    onClick: 'ON_CLICK',
    onBlur: 'ON_BLUR',
    onSave: 'ON_SAVE',
    onChange: 'ON_CHANGE',
  }

  getState = () => this.state

  onClick = () => this.handleStateChange({
    type: BluredInput.stateChangeTypes.onClick,
    input: this.props.initialValue,
    active: true,
  })

  onChange = (e) => this.handleStateChange({
    type: BluredInput.stateChangeTypes.onChange,
    input: e.target.value,
  })

  onBlur = () => {
    this._timeout = setTimeout(() => {
      this.handleStateChange({
        type: BluredInput.stateChangeTypes.onBlur,
        active: false,
        input: '',
      })
    })
  }

  onSubmit = () => {
    const { input } = this.state
    const _input = input.trim()
    this.handleStateChange({
      type: BluredInput.stateChangeTypes.onSave,
      active: false,
      input: _input,
    })
    return _input
  }

  disableBlur = (e) => {
    e.preventDefault()
    clearTimeout(this._timeout)
  }

  handleStateChange = (changes, callback) => {
    this.setState(state => {
      const newState = typeof changes === 'function' ? changes(state) : changes
      const reducedState = this.props.stateReducer(state, newState) || {}
      const { type: ignoredType, ...rest } = reducedState
      return Object.keys(rest).length ? rest : null
    }, callback)
  }

  getRenderMethods = () => ({
    active: this.state.active,
    input: this.state.input,
    getState: this.getState,
    onClick: this.onClick,
    onChange: this.onChange,
    onBlur: this.onBlur,
    onSubmit: this.onSubmit,
    disableBlur: this.disableBlur,
  })

  render () {
    return this.props.children(this.getRenderMethods())
  }
}

BluredInput.propTypes = {
  initialValue: string,
  stateReducer: func,
  children: func,
}

BluredInput.defaultProps = {
  initialValue: '',
  stateReducer: (state, changes) => changes,
}
