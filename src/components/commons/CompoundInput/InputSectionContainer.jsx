import React from 'react'
import { func } from 'prop-types'
import { connect } from 'react-redux'
import { addList } from '../../actions/lists'
import generateId from '../../utils/randomId'
import InputSectionComponent from './CompoundInput'

class InputSection extends React.Component {
  handleSubmit = (input) => {
    this.setState({ active: false })
    this.props.addList({
      listId: generateId(),
      listName: input,
      boardId: 'my_board',
    })
  }

  render () {
    const { children } = this.props
    return (
      <InputSectionComponent
        {...this.props}
        handleSubmit={this.handleSubmit}
      >
        {children}
      </InputSectionComponent>
    )
  }
}

InputSection.propTypes = {
  addList: func.isRequired,
}

const mapDispatchToProps = {
  addList,
}

export default connect(null, mapDispatchToProps)(InputSection)
