import React from 'react'
import { func, any } from 'prop-types'
import styled from 'styled-components'
import memoize from 'lodash.memoize'
import BluredInput from '../BluredInput'
import CompoundContext from './CompoundContext'
import _Placeholder from './_Placeholder'
import _Input from './_Input'
import _Textarea from './_Textarea'
import _SubmitButton from './_SubmitButton'
import _CloseButton from './_CloseButton'

const validate = value => value.length > 0 && value.length <= 30

class CompoundInput extends React.Component {
  static Placeholder = _Placeholder
  static Input = _Input
  static Textarea = _Textarea
  static SubmitButton = _SubmitButton
  static CloseButton = _CloseButton

  handleInput = (state, changes) => {
    switch (changes.type) {
      case BluredInput.stateChangeTypes.onSave:
        const reducedChanges = { ...changes, input: '' }
        return reducedChanges
      default:
        return changes
    }
  }

  withSave = memoize((submitHandler, getState) => {
    console.log('new instance')
    return e => {
      const { input } = getState()
      const _input = input.trim()
      if (!validate(_input)) return
      this.props.handleSubmit(_input)
      submitHandler(e)
    }
  })

  withEnter = memoize(f => e => e.key === 'Enter' && f(e))

  render () {
    const { children } = this.props
    return (
      <Container>
        <BluredInput stateReducer={this.handleInput}>
          {({ active, input, getState, onClick, disableBlur, onSubmit, ...rest }) => (
            <CompoundContext.Provider value={{
              onSave: this.withSave(onSubmit, getState),
              onKeyPress: this.withEnter(this.withSave(onSubmit, getState)),
              active,
              input,
              getState,
              onClick,
              disableBlur,
              onSubmit,
              ...rest }}
            >
              {children}
            </CompoundContext.Provider>
          )}
        </BluredInput>
      </Container>
    )
  }
}

CompoundInput.propTypes = {
  children: any,
  handleSubmit: func,
}

CompoundInput.defaultProps = {
  handleSubmit: () => {},
}

const Container = styled.div``

export default CompoundInput
