import React from 'react'
import { storiesOf } from '@storybook/react'
import CompoundInput from './CompoundInput'

storiesOf('Input Section', module)
  .add('Compund Component for input section without styles only bussines logic', () => (
    <CompoundInput>
      <CompoundInput.Input>
        <CompoundInput.SubmitButton><button>save</button></CompoundInput.SubmitButton>
        <CompoundInput.Textarea><textarea /></CompoundInput.Textarea>
        <CompoundInput.CloseButton><button>close</button></CompoundInput.CloseButton>
      </CompoundInput.Input>
      <CompoundInput.Placeholder><div>Placeholder</div></CompoundInput.Placeholder>
    </CompoundInput>
  ))
