import React from 'react'
import { any } from 'prop-types'
import CompoundContext from './CompoundContext'

export default class _Input extends React.PureComponent {
  static propTypes = {
    children: any,
  }
  render () {
    const { children } = this.props
    return (
      <CompoundContext.Consumer>
        {({ active }) => (
          active ? children : null
        )}
      </CompoundContext.Consumer>
    )
  }
}
