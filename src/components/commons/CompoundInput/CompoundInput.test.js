import React from 'react'
import styled from 'styled-components'
import { cleanup, fireEvent, waitForElement, render } from 'react-testing-library'
import CompoundInput from './CompoundInput'

const Textarea = styled('textarea')``
const Button = styled('button')``
const Placeholder = styled('div')``

afterEach(cleanup)

test('List input container for compound input should render correctly', async () => {
  const spyFn = jest.fn()

  const { container, queryByText, queryByPlaceholderText } = render(
    <CompoundInput handleSubmit={spyFn}>
      <CompoundInput.Input>
        <CompoundInput.SubmitButton><Button>save</Button></CompoundInput.SubmitButton>
        <CompoundInput.Textarea><Textarea placeholder="placeholder"/></CompoundInput.Textarea>
        <CompoundInput.CloseButton><Button>close</Button></CompoundInput.CloseButton>
      </CompoundInput.Input>
      <CompoundInput.Placeholder><Placeholder>Placeholder</Placeholder></CompoundInput.Placeholder>
    </CompoundInput>
  )
  expect(container.firstChild).toMatchSnapshot()

  // User initially should see only placeholder inside <CompoundInput.Placeholder> tag
  const placeholder = queryByText('Placeholder')
  expect(placeholder).toBeInTheDocument()

  // User can click on placeholder and in elements inside <CompoundInput.Input> tag
  fireEvent.click(placeholder)
  const textarea = queryByPlaceholderText('placeholder')
  expect(textarea).toBeInTheDocument()

  const saveButton = queryByText('save')
  expect(saveButton).toBeInTheDocument()

  const closeButton = queryByText('close')
  expect(closeButton).toBeInTheDocument()

  // user write to textarea field
  const input = 'my awesome text'
  fireEvent.change(textarea, { target: { value: input } })
  expect(queryByText(input)).toBeInTheDocument()
  fireEvent.click(saveButton)

  // user can save his input
  await waitForElement(() => queryByText(/Placeholder/))
  expect(spyFn).toBeCalledTimes(1)
})
