import React from 'react'
import { any } from 'prop-types'
import CompoundContext from './CompoundContext'

export default class _SubmitButton extends React.PureComponent {
  static propTypes = {
    children: any,
  }
  render () {
    const { children } = this.props
    return (
      <CompoundContext.Consumer>
        {({ disableBlur, onSave }) =>
          React.cloneElement(children, {
            onClick: onSave,
            onMouseDown: disableBlur,
          })}
      </CompoundContext.Consumer>
    )
  }
}
