import React from 'react'
import { any } from 'prop-types'
import CompoundContext from './CompoundContext'

export default class _Textarea extends React.PureComponent {
  static propTypes = {
    children: any,
  }
  render () {
    const { children } = this.props
    return (
      <CompoundContext.Consumer>
        {({ input, onKeyPress, onClick, ...rest }) => React.cloneElement(children, {
          value: input,
          autoFocus: true,
          onKeyPress,
          ...rest,
        })}
      </CompoundContext.Consumer>
    )
  }
}
