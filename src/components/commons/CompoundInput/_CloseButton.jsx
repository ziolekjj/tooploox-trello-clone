import React from 'react'
import { any } from 'prop-types'

export default class _CloseButton extends React.PureComponent {
  static propTypes = {
    children: any,
  }
  render () {
    return this.props.children
  }
}
