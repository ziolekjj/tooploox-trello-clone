import React from 'react'
import { cleanup, render, fireEvent } from 'react-testing-library'
import { StaticRouter } from 'react-router-dom'
import Header from './Header'

afterEach(cleanup)

test('Click on header title links to main site', () => {
  const context = {}
  const { queryByText } = render(
    <StaticRouter context={context}>
      <Header />
    </StaticRouter>
  )
  const HomeText = queryByText(/Trello Clone/)
  fireEvent.click(HomeText)
  expect(context).toHaveProperty('action', 'PUSH')
  expect(context).toHaveProperty('url', '/')
})

test('Click on header title links to main site', () => {
  const context = {}
  const { queryByTestId } = render(
    <StaticRouter context={context}>
      <Header />
    </StaticRouter>
  )
  const HomeIcon = queryByTestId('_HomeIcon')
  fireEvent.click(HomeIcon)
  expect(context).toHaveProperty('action', 'PUSH')
  expect(context).toHaveProperty('url', '/')
})

test('Header component snapshots', () => {
  const context = {}
  const { container } = render(
    <StaticRouter context={context}>
      <Header />
    </StaticRouter>
  )
  expect(container.firstChild).toMatchSnapshot()
})
