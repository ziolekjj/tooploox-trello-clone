import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { TiHome } from 'react-icons/ti'

const Header = () => (
  <Container>
    <StyledIconLink to="/"><TiHome size={18} data-testid="_HomeIcon" /></StyledIconLink>
    <AppName>
      <StyledLink to="/">Trello Clone</StyledLink>
    </AppName>
  </Container>
)

const Container = styled.header`
  display:flex;
  align-items: center;
  justify-content:center;
  box-sizing: border-box;
  background-color: rgba(0,0,0,.15);
  height: 40px;
  line-height: 32px;
  color: white;
  font-family: 'Helvetica Neue', Arial, Helvetica, sans-serif;
  text-align: center;
  vertical-align: middle;
`

const StyledIconLink = styled(Link)`
  width: 20px;
  padding: 5px;
  margin: 10px 5px;
  display:block;
  color: white;
  text-decoration: none;
  background-color: rgba(255,255,255, .3);
  border-radius: 3px;

  &:hover {
    background-color: rgba(255,255,255, .2);
  }
`

const StyledLink = styled(Link)`
  color: rgba(255,255,255, .5);
  text-decoration: none;

  &:hover {
    color: rgba(255,255,255, 1);
  }
`

const AppName = styled.div`
  flex:9;
  color: white;
  font-size: 20px;
  line-height: 24px;
`

export default Header
