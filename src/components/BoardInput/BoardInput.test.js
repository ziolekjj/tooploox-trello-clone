import React from 'react'
import { cleanup, fireEvent, waitForElement } from 'react-testing-library'
import BoardInput from './BoardInput'
import { render } from '../../TestWrapper'

const testStore = {
  boards: { my_board: { id: 'my_board', boardName: 'My board', lists: ['my_list'] } },
  lists: { my_list: { id: 'my_list', listName: 'My List', tasks: [] } },
  tasks: {},
}

jest.mock('react-beautiful-dnd', () => ({
  Droppable: jest.fn(
    ({ children }) => children({}, {})
  ),
  Draggable: jest.fn(
    ({ children }) => children({}, {})
  ),
}))

afterEach(cleanup)

test('List input container for compound input should render correctly', async () => {
  const { container, queryByText, queryByPlaceholderText, store } = render(<BoardInput />, { initialState: testStore })

  // User should see placeholder with `Add a board...` text
  const placeholder = queryByText(/Add a board.../)
  expect(placeholder).toBeInTheDocument()
  expect(container.firstChild).toMatchSnapshot()

  // and should be able to click on it...
  fireEvent.click(placeholder)
  const textarea = queryByPlaceholderText('Add a board...')
  expect(textarea).toBeInTheDocument()
  expect(container.firstChild).toMatchSnapshot()

  // user can input some text
  const input = 'New board'
  fireEvent.change(textarea, { target: { value: input } })
  expect(queryByText(input)).toBeInTheDocument()

  // user can save his input by pressing on save button
  fireEvent.click(queryByText('save'))
  await waitForElement(() => queryByText(/Add a board/))
  expect(queryByText(/Add a board/)).toBeInTheDocument()
  expect(container.firstChild).toMatchSnapshot()

  // check if redux handlers works properly
  const state = store.getState()
  const boardsIdArray = Object.keys(state.boards)
  expect(boardsIdArray).toHaveLength(2)
  expect(state.boards[boardsIdArray[1]]).toHaveProperty('boardName', 'New board')
})
