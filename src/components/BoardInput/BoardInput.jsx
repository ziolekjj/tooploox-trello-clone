import React from 'react'
import { func } from 'prop-types'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { FiX } from 'react-icons/fi'
import { createBoard } from '../../actions/boards'
import generateId from '../../utils/randomId'
import SubmitButton from '../SubmitButton'
import CompoundInput from '../commons/CompoundInput'

class BoardInput extends React.Component {
  handleSubmit = (input) => {
    this.setState({ active: false })
    this.props.createBoard({
      id: generateId(),
      boardName: input,
    })
  }

  render () {
    return (
      <CompoundInput handleSubmit={this.handleSubmit}>
        <CompoundInput.Input>
          <InputContainer>
            <CompoundInput.Textarea><Textarea placeholder="Add a board..." /></CompoundInput.Textarea>
            <CompoundInput.SubmitButton><SubmitButton>save</SubmitButton></CompoundInput.SubmitButton>
            <CompoundInput.CloseButton><StyledFiX size={20}/></CompoundInput.CloseButton>
          </InputContainer>
        </CompoundInput.Input>
        <CompoundInput.Placeholder>
          <Placeholder>Add a board...</Placeholder>
        </CompoundInput.Placeholder>
      </CompoundInput>
    )
  }
}

BoardInput.propTypes = {
  createBoard: func.isRequired,
}

const InputContainer = styled.div`
  padding: 5px;
  height: 70px;
  border-radius: 3px;
  background-color: #dfe3e6;
  box-shadow: 0 1px 0 0 rgba(0, 0, 0, 0.4);
`

const Textarea = styled.textarea`
  box-sizing: border-box;
  height: 34px;
  font-size: 16px;
  line-height: unset;
  width: 100%;
  border: 2px solid #5ba4cf;
  border-radius: 3px;
  box-shadow: 0 1px 0 0 rgba(0, 0, 0, 0.2);
  padding: 5px;
  margin-bottom: 10px;
  overflow: hidden;
  white-space: nowrap;
  color: #777;
  margin-bottom: 3px;
  resize: none;
  outline: none;
`

const Placeholder = styled.div`
  box-sizing: border-box;
  padding: 0 4px;
  width: 100%;
  height: 80px;
  line-height: 30px;
  background-color: rgba(0,0,0, .25);
  border-radius: 3px;
  color: rgba(255, 255, 255, 1);
  cursor: pointer;
  transition: opacity 1s ease;
  &:hover {
    background-color: rgba(0,0,0, .4);
  }
`

const StyledFiX = styled(FiX)`
  color: #777;
  cursor: pointer;
  vertical-align: middle;
  &:hover {
    color: #999;
  }
`

const mapDispatchToProps = {
  createBoard,
}

export default connect(null, mapDispatchToProps)(BoardInput)
