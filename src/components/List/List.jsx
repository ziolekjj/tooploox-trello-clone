import React from 'react'
import { arrayOf, shape, string, number, func } from 'prop-types'
import { Droppable, Draggable } from 'react-beautiful-dnd'
import styled from 'styled-components'

import Task from '../Task'
import TaskInput from './TaskInput'
import ListTitleInput from './ListTitleInput'

class List extends React.Component {
  render () {
    const { id, index, listName, tasks, addNewTask, handleOnBlur, editListTitle } = this.props
    return (
      <Container >
        <Draggable draggableId={id} index={index}>
          {provided => (
            <Box {...provided.draggableProps} {...provided.dragHandleProps} innerRef={provided.innerRef}>
              <ListTitleInput
                handleEditClick={this.props.handleEditClick}
                onSubmit={editListTitle}
              >
                {listName}
              </ListTitleInput>
              <Droppable droppableId={id}>
                {provided => (
                  <Entries
                    innerRef={provided.innerRef}
                    {...provided.droppableProps}
                  >
                    {tasks.map((id, index) => (
                      <Task index={index} key={id} id={id} />
                    ))}
                    {provided.placeholder}
                  </Entries>
                )}
              </Droppable>
              <TaskInput
                onClick={this.props.handleAddClick}
                onSubmit={addNewTask}
                onBlur={handleOnBlur}
              >Add a card...</TaskInput>
            </Box>
          )}
        </Draggable>
      </Container>
    )
  }
}

List.propTypes = {
  id: string.isRequired,
  listName: string.isRequired,
  entries: arrayOf(shape({
    id: string,
    index: number,
    description: string,
  })),
  editListTitle: func,
  addNewTask: func,
  handleEditClick: func,
  handleOnBlur: func,
}

List.defaultProps = {
  entries: [],
  editListTitle: () => {},
  addNewTask: () => {},
  handleEditClick: () => {},
  handleOnBlur: () => {},
}

const Container = styled.div`
  min-width: 290px;
`

const Box = styled.div`
  min-width: 280px;
  margin: 5px;
  font-size: 14px;
  line-height: 20px;
  background-color: #dfe3e6;
  box-shadow: 0 1px 0 0  rgba(0, 0, 0, .4);
  border-radius: 3px;
  vertical-align: top;
`

const Entries = styled.div`
  display: flex;
  flex-direction: column;
  padding: 5px;
`

export default List
