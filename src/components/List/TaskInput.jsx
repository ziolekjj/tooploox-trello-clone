import React from 'react'
import { func } from 'prop-types'
import styled from 'styled-components'
import { FiX } from 'react-icons/fi'
import memoize from 'lodash.memoize'
import BluredInput from '../commons/BluredInput'
import SubmitButton from '../SubmitButton'
import HeightObserver from '../commons/HeightObserver'

class Input extends React.Component {
  inputReducer = (state, changes) => {
    if (changes.type === BluredInput.stateChangeTypes.onSave) {
      if (!changes.input.length) {
        return {
          ...changes,
          active: true,
        }
      }
    }
    return changes
  }
  withSave = memoize((eventHandler, getState) => event => {
    const { input } = getState()
    if (input.trim()) this.props.onSubmit({ description: input })
    eventHandler(event)
  })

  onKeyPress = memoize(f => e => e.key === 'Enter' && !e.shiftKey && f(e))

  render () {
    return (
      <BluredInput stateReducer={this.inputReducer}>
        {({ active, input, getState, onClick, onSubmit, disableBlur, ...rest }) => {
          if (!active) return <InputButton onClick={onClick}>Add a card...</InputButton>
          return (
            <Container>
              <HeightObserver>{
                ({ height, ref }) => (
                  <Textarea
                    type="text"
                    height={height}
                    innerRef={ref}
                    autoFocus
                    value={input}
                    onKeyPress={this.onKeyPress(this.withSave(onSubmit, getState))}
                    data-testid="_ListTaskTextarea"
                    {...rest}
                  />
                )
              }
              </HeightObserver>
              <SubmitButton
                onMouseDown={disableBlur}
                onClick={this.withSave(onSubmit, getState)}
                data-testid="_ListSubmitButton"
              >
                Add
              </SubmitButton>
              <StyledFiX size={22} />
            </Container>
          )
        }}
      </BluredInput>
    )
  }
}

Input.propTypes = {
  onSubmit: func,
}

Input.defaultProps = {
  onClick: () => {},
  onSubmit: () => {},
}

const Container = styled.div`
  box-sizing: border-box;
  display: flex;
  padding: 0 10px 5px 10px;
  flex-wrap: wrap;
  align-items: center;
`

const Textarea = styled.textarea`
  box-sizing: border-box;
  font-size: 14px;
  line-height: 20px;
  color: #777;
  font-family: 'Helvetica Neue', Arial, Helvetica, sans-serif;
  padding: 5px;
  margin-bottom: 5px;
  width: 100%;
  min-height: 70px;
  height: ${props => `${props.height}px`};
  background-color: white;
  box-shadow: 0 1px 0 0 rgba(0, 0, 0, 0.2);
  border-radius: 3px;
  overflow: hidden scroll;
  overflow-wrap: break-word;
  border: none;
  resize: none;
  outline: none;
`

const InputButton = styled.div`
  padding: 10px;
  color: #aaa;
  cursor: pointer;

  &:hover {
    border-radius: 3px;
    background-color: rgb(110, 120, 140, 0.2);
    color: #555;
    text-decoration: underline;
  }
`

const StyledFiX = styled(FiX)`
  color: #777;
  cursor: pointer;

  &:hover {
    color: #999;
  }
`

export default Input
