import React from 'react'
import { string, func } from 'prop-types'
import styled from 'styled-components'
import { FiMoreHorizontal } from 'react-icons/fi'
import memoize from 'lodash.memoize'
import BluredInput from '../commons/BluredInput'

const validate = value => value.length > 0 && value.length <= 30

class ListTitleInput extends React.Component {
  withSave = memoize((eventHandler, getState) => event => {
    const { input } = getState()
    const _input = input.trim()
    if (validate(_input)) this.props.onSubmit({ listName: _input })
    eventHandler(event)
  })

  onFocus = e => e.target.select()

  withEnter = memoize(f => e => e.key === 'Enter' && f(e))

  render () {
    const { children } = this.props
    return (
      <Container>
        <BluredInput initialValue={children}>
          {({ active, input, getState, onBlur, onSubmit, onClick, ...rest }) => (
            <React.Fragment>
              {active ? (
                <Textarea
                  autoFocus
                  value={input}
                  onBlur={this.withSave(onBlur, getState)}
                  onKeyPress={this.withEnter(this.withSave(onBlur, getState))}
                  onFocus={this.onFocus}
                  data-testid="_ListTitleTextarea"
                  {...rest}
                />
              ) : (
                <Title onClick={onClick} data-testid="_ListTitle">{children}</Title>
              )}
            </React.Fragment>
          )}
        </BluredInput>
        <StyledMoreIcon />
      </Container>
    )
  }
}

ListTitleInput.propTypes = {
  children: string,
  handleNameChange: func,
}

ListTitleInput.defaultProps = {
  children: string,
  handleNameChange: null,
}

const Container = styled.div`
  box-sizing: border-box;
  height: 44px;
  padding: 10px;
  display: flex;
  justify-content: space-between;
  color: #555;
  cursor: pointer;
`

const Title = styled.h2`
  font-size: 14px;
  line-height: 24px;
  font-weight: 700;
  height: 24px;
  width: calc(100% - 20px);
  margin-top: 2px;
`

const Textarea = styled.textarea`
  box-sizing: border-box;
  padding: 4px;
  height: 32px;
  line-height: unset;
  font-size: 14px;
  width: 100%;
  box-shadow: 0 1px 0 0 rgba(0, 0, 0, 0.2);
  border: 2px solid #5ba4cf;
  border-radius: 3px;
  overflow: hidden scroll;
  overflow-wrap: break-word;
  resize: none;
  outline: none;
  color: #777;
`

const StyledMoreIcon = styled(FiMoreHorizontal)`
  padding: 4px;
  width: 20px;
  height: 20px;
  &:hover {
    border-radius: 3px;
    background-color: rgb(100, 120, 140, 0.2);
  }
`
export default ListTitleInput
