import React from 'react'
import { string, func, arrayOf } from 'prop-types'
import { connect } from 'react-redux'
import { editList } from '../../actions/lists'
import { addTask } from '../../actions/tasks'
import generateId from '../../utils/randomId'
import ListComponent from './List'

class ListContainer extends React.Component {
  editListTitle = ({ listName }) => {
    const { id, editList } = this.props
    editList({ id, listName: listName })
  }

  addNewTask = ({ description }) => {
    const { addTask, id } = this.props
    addTask({ listId: id, id: generateId(), description })
    this.setState({ activeInput: false })
  }

  render () {
    const { id, listName, tasks, ...rest } = this.props
    return (
      <ListComponent
        id={id}
        listName={listName}
        tasks={tasks}
        editListTitle={this.editListTitle}
        handleEditClick={this.handleEditClick}
        handleAddClick={this.handleAddClick}
        addNewTask={this.addNewTask}
        handleOnBlur={this.handleOnBlur}
        {...rest}
      />
    )
  }
}

ListContainer.propTypes = {
  id: string.isRequired,
  listName: string.isRequired,
  tasks: arrayOf(string),
  editList: func.isRequired,
  addTask: func.isRequired,
}

const mapStateToProps = (state, props) => ({
  ...state.lists[props.id],
})

const mapDispatchToProps = {
  editList,
  addTask,
}

export default connect(mapStateToProps, mapDispatchToProps)(ListContainer)
