import React from 'react'
import { cleanup, fireEvent, waitForElement } from 'react-testing-library'
import List from './'
import { render } from '../../TestWrapper'

const testStore = {
  boards: { my_board: { id: 'my_board', boardName: 'My board', lists: ['my_list'] } },
  lists: { my_list: { id: 'my_list', listName: 'My List', tasks: [] } },
  tasks: {},
}

jest.mock('react-beautiful-dnd', () => ({
  Droppable: jest.fn(
    ({ children }) => children({}, {})
  ),
  Draggable: jest.fn(
    ({ children }) => children({}, {})
  ),
}))

afterEach(cleanup)

describe('List component stories', () => {
  test('User can add new task entries to the list and see them', () => {
    const LIST_ID = 'my_list'
    const { container, queryByText, queryByTestId, queryAllByTestId, store } = render(<List id="my_list" />, { initialState: testStore })

    // Initial component should not have any task antries and active textareas in it...
    expect(container.firstChild).toMatchSnapshot()
    expect(queryByText(store.getState().lists[LIST_ID].listName)).toBeInTheDocument()
    expect(queryByText('Add a card...')).toBeInTheDocument()
    expect(queryByTestId('_ListTaskTextarea')).not.toBeInTheDocument()
    expect(queryByTestId('_ListTaskTitleTextarea')).not.toBeInTheDocument()

    // After clicking on "Add a card" button user should see new textarea field.
    // Others fields should look same as before...
    fireEvent.click(queryByText(/Add a card/))
    expect(queryByText(/Add a card/)).not.toBeInTheDocument()
    expect(queryByTestId('_ListTaskTextarea')).toBeInTheDocument()
    expect(queryByText(/Add/)).toBeInTheDocument()
    expect(queryByTestId('_ListTaskTitleTextarea')).not.toBeInTheDocument()
    expect(container.firstChild).toMatchSnapshot()

    // User can input some value into textarea and save it
    const input = queryByTestId('_ListTaskTextarea')
    const inputValue = 'Napiszę dzisiaj jakiś test :-)'
    fireEvent.change(input, { target: { value: inputValue } })
    expect(queryByText(inputValue)).toBeInTheDocument()
    expect(container.firstChild).toMatchSnapshot()

    // After saving newly created task, user should be able to see it on a list
    fireEvent.click(queryByText(/Add/))
    expect(queryByText(inputValue)).toBeInTheDocument()
    expect(queryByText(/Add a card.../)).toBeInTheDocument()
    expect(queryByTestId('_TaskEntryNonEditable')).toBeInTheDocument()
    expect(container.firstChild).toMatchSnapshot()

    // User can add more tasks!
    fireEvent.click(queryByText(/Add a card/))
    const otherInput = queryByTestId('_ListTaskTextarea')
    fireEvent.change(otherInput, { target: { value: 'Task 2' } })
    fireEvent.click(queryByText(/Add/))
    const submittedTasks = queryAllByTestId('_TaskEntryNonEditable')
    expect(submittedTasks).toHaveLength(2)

    // But, he can not add a task without description!
    fireEvent.click(queryByText(/Add a card/))
    const emptyInput = queryByTestId('_ListTaskTextarea')
    fireEvent.change(emptyInput, { target: { value: '' } })
    fireEvent.click(queryByText(/Add/))
    expect(queryByText(/Add a card.../)).not.toBeInTheDocument()
  })

  test('User can edit descriptions of added tasks', async () => {
    const { queryByText, queryByTestId } = render(<List id="my_list" />, { initialState: testStore })

    // Obviously user need to add new task entry to be able to edit it.
    fireEvent.click(queryByText(/Add a card/))
    const input = queryByTestId('_ListTaskTextarea')
    const inputValue = 'Napiszę dzisiaj jakiś test :-)'
    fireEvent.change(input, { target: { value: inputValue } })
    fireEvent.click(queryByText(/Add/))
    const addedTask = queryByText(inputValue)
    expect(addedTask).toBeInTheDocument()

    // After click on entry user should see new textarea field
    fireEvent.click(addedTask)
    const textarea = queryByTestId('_TaskEntryEditable')
    expect(textarea).toBeInTheDocument()

    // User can input some new values to newly created task
    const newInput = 'Nowa wartość opisu dla mojego zadania'
    fireEvent.change(textarea, { target: { value: newInput } })
    fireEvent.keyPress(textarea, { key: 'Enter', keyCode: 13, which: 13 })

    // And see newly created changes...
    const editedInput = await waitForElement(() => queryByTestId('_TaskEntryNonEditable'))
    expect(editedInput).toBeInTheDocument()
    expect(queryByText(newInput)).toBeInTheDocument()
  })
  test('User can edit his list title', async () => {
    const LIST_ID = 'my_list'
    const { queryByText, queryByTestId, store } = render(<List id="my_list" />, { initialState: testStore })

    // User can click on list title to edit it and see textarea field
    fireEvent.click(queryByText(store.getState().lists[LIST_ID].listName))
    const textarea = queryByTestId('_ListTitleTextarea')
    expect(textarea).toBeInTheDocument()

    // User can change values in list title textarea field and submit them
    const newTitle = 'Your List'
    fireEvent.change(textarea, { target: { value: newTitle } })
    fireEvent.blur(textarea)

    // User can see edited title
    const editedTitle = await waitForElement(() => queryByTestId('_ListTitle'))
    expect(editedTitle).toBeInTheDocument()
    expect(queryByText(newTitle)).toBeInTheDocument()
  })
})
