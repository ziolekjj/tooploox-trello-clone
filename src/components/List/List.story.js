import React from 'react'
import { storiesOf } from '@storybook/react'
import { DragDropContext, Droppable } from 'react-beautiful-dnd'
import { Provider } from 'react-redux'
import initStore from '../../store'
import ListContainer from './ListContainer'

storiesOf('List', module)
  .add('List container', () => {
    const store = initStore({
      initialState: {
        boards: {
          'my_board': { id: 'my_board', boardName: 'My Board', lists: ['012'] },
        },
        lists: {
          '012': { id: '012', listName: 'Backlog', tasks: ['0', '1', '2'] },
        },
        tasks: {
          '0': { id: '0', description: 'Task 1' },
          '1': { id: '1', description: 'Task 2' },
          '2': { id: '2', description: 'Task 3' },
        },
      },
    })
    return (
      <Provider store={store}>
        <DragDropContext
          onDragEnd={() => {}}
        >
          <Droppable droppableId='board'>
            {provided =>
              <div style={{ boxSizing: 'border-box', position: 'absolute' }}
                {...provided.droppableProps}
                ref={provided.innerRef}
              >
                <ListContainer id="012" />
                {provided.placeholder}
              </div>
            }
          </Droppable>
        </DragDropContext>
      </Provider>
    )
  })
