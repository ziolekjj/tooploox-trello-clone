import React from 'react'
import { string, number, func, object } from 'prop-types'
import { connect } from 'react-redux'
import { editTask } from '../../actions/tasks'
import Task from './Task'

class TaskContainer extends React.Component {
  render () {
    const { id, index, entry, editTask } = this.props
    return (
      <Task
        id={id}
        index={index}
        onSubmit={editTask}
      >
        {entry.description}
      </Task>
    )
  }
}

TaskContainer.propTypes = {
  id: string.isRequired,
  index: number.isRequired,
  editTask: func.isRequired,
  entry: object.isRequired,
}

const mapStateToProps = (state, props) => ({
  entry: state.tasks[props.id],
})

const mapDispatchToProps = {
  editTask,
}

export default connect(mapStateToProps, mapDispatchToProps)(TaskContainer)
