import React from 'react'
import { string, number } from 'prop-types'
import styled from 'styled-components'
import { Draggable } from 'react-beautiful-dnd'
import memoize from 'lodash.memoize'
import HeightObserver from '../commons/HeightObserver'
import BluredInput from '../commons/BluredInput'

class Task extends React.Component {
  withSave = memoize((eventHandler, getState) => event => {
    const { input } = getState()
    if (input.trim()) this.props.onSubmit({ id: this.props.id, description: input })
    eventHandler(event)
  })

  onFocus = e => e.target.select()

  onKeyPress = memoize(f => e => e.key === 'Enter' && !e.shiftKey && f(e))

  render () {
    const { children, id, index } = this.props
    return (
      <Draggable draggableId={id} index={index}>
        {provided => (
          <BluredInput initialValue={children}>
            {({ active, input, getState, onClick, onBlur, ...rest }) => (
              <React.Fragment>
                {active ? (
                  <HeightObserver>
                    {({ height, ref }) => (
                      <Textarea
                        height={height}
                        innerRef={ref}
                        autoFocus
                        value={input}
                        onBlur={this.withSave(onBlur, getState)}
                        onFocus={this.onFocus}
                        onKeyPress={this.onKeyPress(this.withSave(onBlur, getState))}
                        data-testid="_TaskEntryEditable"
                        {...rest}
                      />
                    )}
                  </HeightObserver>
                ) : (
                  <Container
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                    innerRef={provided.innerRef}
                    onClick={onClick}
                    data-testid="_TaskEntryNonEditable"
                  >
                    {children}
                  </Container>
                )}
              </React.Fragment>
            )}
          </BluredInput>
        )}
      </Draggable>
    )
  }
}

Task.propTypes = {
  children: string,
  id: string,
  index: number,
}

const Container = styled.div`
  box-sizing: border-box;
  max-width: 260px;
  background-color: #fff;
  box-shadow: 0 1px 0 0 rgba(0, 0, 0, 0.2);
  word-wrap: break-word;
  color: #777;
  border-radius: 3px;
  padding: 5px;
  margin: 5px;
`

const Textarea = styled.textarea`
  box-sizing: border-box;
  max-width: 260px;
  padding: 5px;
  margin: 5px;
  line-height: unset;
  font-size: 14px;
  box-shadow: 0 1px 0 0 rgba(0, 0, 0, 0.2);
  border: 2px solid #5ba4cf;
  border-radius: 3px;
  resize: none;
  outline: none;
  color: #777;
  height: ${props => `${props.height}px`};
`

export default Task
