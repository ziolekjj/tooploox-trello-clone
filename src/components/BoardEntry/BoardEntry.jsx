import React from 'react'
import { string } from 'prop-types'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import media from '../../utils/media'

export default class BoardEntry extends React.Component {
  render () {
    const { children, id } = this.props

    return (
      <Container>
        <StyledLink to={`/boards/${id}`}>
          <EntryTitle>
            {children}
          </EntryTitle>
        </StyledLink>
      </Container>
    )
  }
};

BoardEntry.propTypes = {
  id: string.isRequired,
  children: string.isRequired,
}

const StyledLink = styled(Link)`
  display:block;
  height: 100%;
  text-decoration: none;
`

const Container = styled.div`
  cursor:pointer;
  overflow: hidden;
  box-sizing: border-box;
  width: calc(25% - 10px);
  ${media.third`
    width: calc(33% - 10px);
  `};
  ${media.half`
    width: calc(50% - 10px);
  `};
  height: 80px;
  margin: 5px;
  background-color: #0079bf;
  border-radius: 3px;

  &:hover {
    background-color: #0D5B8E;
  }
`

const EntryTitle = styled.h3`
  margin: 0;
  padding: 5px;
  color: white;
  word-wrap: break-word;
  font-weight: 700;
  font-size: 16px;
`
