import React from 'react'
import { storiesOf } from '@storybook/react'
import { StaticRouter } from 'react-router-dom'
import BoardEntry from '../BoardEntry'

storiesOf('Board Entry', module)
  .add('Standard Board Entry component', () => (
    <StaticRouter context={{}}>
      <BoardEntry>Board Title</BoardEntry>
    </StaticRouter>
  ))
