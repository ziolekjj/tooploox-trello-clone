import React from 'react'
import { cleanup, render, fireEvent } from 'react-testing-library'
import { StaticRouter } from 'react-router-dom'
import BoardEntry from './BoardEntry'

afterEach(cleanup)

test('Click on header title links to main site', () => {
  const context = {}
  const id = '123'
  const { queryByText, container } = render(
    <StaticRouter context={context}>
      <BoardEntry id={id}>Board</BoardEntry>
    </StaticRouter>
  )

  // User can click into a board to see board page
  const boardEntry = queryByText(/Board/)
  fireEvent.click(boardEntry)
  expect(context).toHaveProperty('action', 'PUSH')
  expect(context).toHaveProperty('url', `/boards/${id}`)
  expect(container.firstChild).toMatchSnapshot()
})
