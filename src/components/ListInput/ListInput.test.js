import React from 'react'
import { cleanup, fireEvent, waitForElement } from 'react-testing-library'
import ListInput from './ListInput'
import { render } from '../../TestWrapper'

const testStore = {
  boards: { my_board: { id: 'my_board', boardName: 'My board', lists: ['my_list'] } },
  lists: { my_list: { id: 'my_list', listName: 'My List', tasks: [] } },
  tasks: {},
}

jest.mock('react-beautiful-dnd', () => ({
  Droppable: jest.fn(
    ({ children }) => children({}, {})
  ),
  Draggable: jest.fn(
    ({ children }) => children({}, {})
  ),
}))

afterEach(cleanup)

test('List input container for compound input should render correctly', async () => {
  const boardId = 'my_board'
  const { container, queryByText, queryByPlaceholderText, queryByTestId, store } = render(<ListInput boardId={boardId} />, { initialState: testStore })

  // User should see placeholder with `Add a list...` string
  const placeholder = queryByText(/Add a list.../)
  expect(placeholder).toBeInTheDocument()
  expect(container.firstChild).toMatchSnapshot()

  // User can click on placeholder and in order to see textarea input
  fireEvent.click(placeholder)
  const textarea = queryByPlaceholderText('Add a list...')
  expect(textarea).toBeInTheDocument()

  // User can see save and close button
  const saveButton = queryByText('save')
  const closeButton = queryByTestId('_ListInputCloseButton')
  expect(saveButton).toBeInTheDocument()
  expect(closeButton).toBeInTheDocument()

  // user can input some text
  const input = 'My list title'
  fireEvent.change(textarea, { target: { value: input } })
  expect(queryByText(input)).toBeInTheDocument()

  // user can save his input
  fireEvent.keyPress(textarea, { key: 'Enter', keyCode: 13, which: 13 })
  await waitForElement(() => queryByText(/Add/))
  expect(queryByText(/Add a list/)).toBeInTheDocument()
  expect(container.firstChild).toMatchSnapshot()

  // Beacouse we test it in isolation from other components
  // The only way to check if user list is added is to check it somewhere in redux store
  const state = store.getState()
  expect(state.boards[boardId].lists).toHaveLength(2)
  const listId = state.boards[boardId].lists[1]
  expect(listId).toBeDefined()
  expect(state.lists[listId]).toHaveProperty('listName', input)
})
