import React from 'react'
import { func, string } from 'prop-types'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { FiX } from 'react-icons/fi'
import { addList } from '../../actions/lists'
import generateId from '../../utils/randomId'
import SubmitButton from '../SubmitButton'
import CompoundInput from '../commons/CompoundInput'

class ListInput extends React.Component {
  handleSubmit = (input) => {
    this.props.addList({
      listId: generateId(),
      listName: input,
      boardId: this.props.boardId,
    })
  }

  render () {
    return (
      <CompoundInput handleSubmit={this.handleSubmit}>
        <CompoundInput.Input>
          <InputContainer>
            <CompoundInput.Textarea><Textarea placeholder="Add a list..." /></CompoundInput.Textarea>
            <CompoundInput.SubmitButton><SubmitButton>save</SubmitButton></CompoundInput.SubmitButton>
            <CompoundInput.CloseButton><StyledFiX size={20} data-testid="_ListInputCloseButton"/></CompoundInput.CloseButton>
          </InputContainer>
        </CompoundInput.Input>
        <CompoundInput.Placeholder>
          <Placeholder>Add a list...</Placeholder>
        </CompoundInput.Placeholder>
      </CompoundInput>
    )
  }
}

ListInput.propTypes = {
  addList: func.isRequired,
  boardId: string.isRequired,
}

const InputContainer = styled.div`
  display: flex;
  height: 84px;
  flex-wrap: wrap;
  align-items: center;
  padding: 5px;
  border-radius: 3px;
  background-color: #dfe3e6;
  box-shadow: 0 1px 0 0 rgba(0, 0, 0, 0.4);
`

const Textarea = styled.textarea`
  /* box-sizing: border-box; */
  height: 34px;
  line-height: unset;
  width: 100%;
  border: 2px solid #5ba4cf;
  border-radius: 3px;
  box-shadow: 0 1px 0 0 rgba(0, 0, 0, 0.2);
  padding: 5px;
  margin-bottom: 10px;
  overflow: hidden;
  white-space: nowrap;
  color: #777;
  margin-bottom: 3px;
  resize: none;
  outline: none;
`

const Placeholder = styled.div`
  box-sizing: border-box;
  padding: 0 5px;
  width: 100%;
  height: 40px;
  line-height: 30px;
  background-color: rgba(0,0,0, .2);
  border-radius: 3px;
  color: rgba(255, 255, 255, 1);
  cursor: pointer;

  &:hover {
    background-color: rgba(0,0,0, .3);
  }
`

const StyledFiX = styled(FiX)`
  color: #777;
  cursor: pointer;
  vertical-align: middle;
  &:hover {
    color: #999;
  }
`

const mapDispatchToProps = {
  addList,
}

export default connect(null, mapDispatchToProps)(ListInput)
