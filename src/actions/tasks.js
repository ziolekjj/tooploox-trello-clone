import { CREATE_TASK, EDIT_TASK } from '../constants/actionTypes'
import { addTaskToList } from './lists'

export const createTask = ({ id, description }) => ({
  type: CREATE_TASK,
  data: { id, description },
})

export const editTask = ({ id, description }) => ({
  type: EDIT_TASK,
  data: { id, description },
})

export const addTask = ({ id, listId, description }) => dispatch => {
  dispatch(createTask({ id, description }))
  dispatch(addTaskToList({ taskId: id, listId }))
}
