import { CREATE_BOARD, ADD_LIST_TO_BOARD, EDIT_BOARD, MOVE_LIST } from '../constants/actionTypes'

export const createBoard = ({ id, boardName }) => ({
  type: CREATE_BOARD,
  data: { id, boardName },
})

export const editBoard = ({ id, boardName }) => ({
  type: EDIT_BOARD,
  data: { id, boardName },
})

export const addListToBoard = ({ boardId, listId }) => ({
  type: ADD_LIST_TO_BOARD,
  data: { boardId, listId },
})

export const moveList = ({ boardId, sourceIndex, destinationIndex }) => ({
  type: MOVE_LIST,
  data: { boardId, sourceIndex, destinationIndex },
})
