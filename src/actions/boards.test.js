import { createBoard, editBoard, addListToBoard, moveList } from './boards'
import { CREATE_BOARD, ADD_LIST_TO_BOARD, EDIT_BOARD, MOVE_LIST } from '../constants/actionTypes'

test('createBoard action should create an action to create new board', () => {
  const id = '123'
  const boardName = 'New board'
  const expectedAction = {
    type: CREATE_BOARD,
    data: { id, boardName },
  }
  expect(createBoard({ id, boardName })).toEqual(expectedAction)
})

test('editBoard action should create an action to edit board', () => {
  const id = '345'
  const boardName = 'New board'
  const expectedAction = {
    type: EDIT_BOARD,
    data: { id, boardName },
  }
  expect(editBoard({ id, boardName })).toEqual(expectedAction)
})

test('addListToBoard action should create an action to add list to board', () => {
  const listId = 'my_list'
  const boardId = '123'
  const expectedAction = {
    type: ADD_LIST_TO_BOARD,
    data: { listId, boardId },
  }
  expect(addListToBoard({ boardId, listId })).toEqual(expectedAction)
})

test('moveList action should create an action to move list inside board', () => {
  const sourceIndex = 0
  const destinationIndex = 3
  const boardId = '123'
  const expectedAction = {
    type: MOVE_LIST,
    data: { boardId, sourceIndex, destinationIndex },
  }
  expect(moveList({ boardId, sourceIndex, destinationIndex })).toEqual(expectedAction)
})
