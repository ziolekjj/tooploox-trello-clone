import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { createTask, editTask, addTask } from './tasks'
import { CREATE_TASK, EDIT_TASK, ADD_TASK_TO_LIST } from '../constants/actionTypes'

test('should add a task and attach it to the list', () => {
  const mockStore = configureStore([thunk])
  const store = mockStore()
  const taskId = '0'
  const listId = 'a'
  const description = 'Todo task'

  const expectedCreateTaskAction = {
    type: CREATE_TASK,
    data: { id: taskId, description },
  }
  const expectedAddTaskToListAction = {
    type: ADD_TASK_TO_LIST,
    data: { taskId, listId },
  }

  const expectedPayload = [
    expectedCreateTaskAction,
    expectedAddTaskToListAction,
  ]

  store.dispatch(addTask({ id: taskId, listId, description }))
  const actions = store.getActions()
  expect(actions).toEqual(expectedPayload)
})

test('createTask action should create an action to create a new task', () => {
  const id = '123'
  const description = 'todo task'
  const expectedAction = {
    type: CREATE_TASK,
    data: { id, description },
  }
  expect(createTask({ id, description })).toEqual(expectedAction)
})

test('editTask action should create an action to edit a task', () => {
  const id = '123'
  const description = 'edited description'
  const expectedAction = {
    type: EDIT_TASK,
    data: { id, description },
  }
  expect(editTask({ id, description })).toEqual(expectedAction)
})
