import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { createList, editList, addTaskToList, moveTask, addList } from './lists'
import { CREATE_LIST, EDIT_LIST, ADD_TASK_TO_LIST, MOVE_TASK, ADD_LIST_TO_BOARD } from '../constants/actionTypes'

test('should add a List to the store and attach it to the board', () => {
  const mockStore = configureStore([thunk])
  const store = mockStore()
  const listId = 0
  const boardId = 1
  const listName = 'My list name'

  const expectedCreateListAction = {
    type: CREATE_LIST,
    data: { id: listId, listName },
  }
  const expectedAddListToBoardAction = {
    type: ADD_LIST_TO_BOARD,
    data: { listId, boardId },
  }

  const expectedPayload = [
    expectedCreateListAction,
    expectedAddListToBoardAction,
  ]

  store.dispatch(addList({ listId, boardId, listName }))
  const actions = store.getActions()
  expect(actions).toEqual(expectedPayload)
})

test('createList action should create an action to create new list', () => {
  const id = '123'
  const listName = 'Lsit title'
  const expectedAction = {
    type: CREATE_LIST,
    data: { id, listName },
  }
  expect(createList({ id, listName })).toEqual(expectedAction)
})

test('editList action should create an action to edit a list', () => {
  const id = '345'
  const listName = 'Some list title'
  const expectedAction = {
    type: EDIT_LIST,
    data: { id, listName },
  }
  expect(editList({ id, listName })).toEqual(expectedAction)
})

test('addTaskToList action should create an action to add a task to list', () => {
  const taskId = '123'
  const listId = '345'
  const expectedAction = {
    type: ADD_TASK_TO_LIST,
    data: { listId, taskId },
  }
  expect(addTaskToList({ listId, taskId })).toEqual(expectedAction)
})

test('moveTask action should create an action to move list inside a list', () => {
  const sourceListId = '123'
  const destinationListId = '345'
  const sourceIndex = 0
  const destinationIndex = 3
  const expectedAction = {
    type: MOVE_TASK,
    data: { sourceListId, destinationListId, sourceIndex, destinationIndex },
  }
  expect(
    moveTask({ sourceListId, destinationListId, sourceIndex, destinationIndex })
  ).toEqual(expectedAction)
})
