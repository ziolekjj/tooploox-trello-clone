import { CREATE_LIST, EDIT_LIST, ADD_TASK_TO_LIST, MOVE_TASK } from '../constants/actionTypes'
import { addListToBoard } from './boards'

export const createList = ({ id, listName }) => ({
  type: CREATE_LIST,
  data: { id, listName },
})

export const editList = ({ id, listName }) => ({
  type: EDIT_LIST,
  data: { id, listName },
})

export const addTaskToList = ({ listId, taskId }) => ({
  type: ADD_TASK_TO_LIST,
  data: { listId, taskId },
})

export const moveTask = ({ sourceListId, destinationListId, sourceIndex, destinationIndex }) => ({
  type: MOVE_TASK,
  data: { sourceListId, destinationListId, sourceIndex, destinationIndex },
})

export const addList = ({ listId, boardId, listName }) => dispatch => {
  dispatch(createList({ id: listId, listName }))
  dispatch(addListToBoard({ listId, boardId }))
}
