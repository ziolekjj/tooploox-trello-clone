import React from 'react'
import { shape, arrayOf, any } from 'prop-types'
import styled from 'styled-components'
import { renderRoutes } from 'react-router-config'
import './styles/normalize'
import Header from './components/Header'

const Root = ({ route }) => (
  <Container>
    <Header />
    <Content>{renderRoutes(route.routes)}</Content>
  </Container>
)

Root.propTypes = {
  route: shape({
    routes: arrayOf(any),
  }),
}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  flex-grow: 1;
  height: 100%;
  background-color: #0079bf;
`

const Content = styled.div`
  position: relative;
  height: 100%;
`

export default Root
