import React from 'react'
import Loadable from 'react-loadable'
import Root from './Root'

const Home = Loadable({
  loader: () => import('./views/Home'),
  loading: () => null,
})

const Board = Loadable({
  loader: () => import('./views/Board'),
  loading: () => null,
})

const NotFound = () => (<div>X</div>)

export default [
  {
    component: Root,
    routes: [
      {
        component: Home,
        path: '/',
        exact: true,
      },
      {
        component: Board,
        path: '/boards/:id',
      },
      {
        component: NotFound,
      },
    ],
  },
]
