import { css } from 'styled-components'
const sizes = {
  third: 1280,
  half: 710,
}
export default Object.keys(sizes).reduce((acc, label) => {
  // https://github.com/styled-components/styled-components/blob/master/docs/tips-and-tricks.md
  acc[label] = (...args) => css`
    @media (max-width: ${sizes[label] / 16}em) {
      ${css(...args)};
    }
  `

  return acc
}, {})
