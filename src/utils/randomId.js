const generateId = () => new Date().getTime().toString() + Math.random().toString().slice(2, 10)

export default generateId
