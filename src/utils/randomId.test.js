import id from './randomId'

test('should return different id each time', () => {
  const firstId = id()
  const secondId = id()
  expect(firstId).not.toEqual(secondId)
})
