const moveElementBetweenArrays =
  ({ sourceArray, destinationArray, sourceIndex, destinationIndex }) => {
    const elementToMove = sourceArray[sourceIndex]
    const newSourceArray = [
      ...sourceArray.slice(0, sourceIndex),
      ...sourceArray.slice(sourceIndex + 1, sourceArray.length),
    ]
    const newDestinationArray = [
      ...destinationArray.slice(0, destinationIndex),
      elementToMove,
      ...destinationArray.slice(destinationIndex, destinationArray.length),
    ]
    return {
      newSourceArray,
      newDestinationArray,
    }
  }

const moveElement = ({
  array,
  sourceIndex,
  destinationIndex,
}) => {
  const elementToMove = array[sourceIndex]
  const arrayWithoutElement = [
    ...array.slice(0, sourceIndex),
    ...array.slice(sourceIndex + 1, array.length),
  ]
  const newArray = [
    ...arrayWithoutElement.slice(0, destinationIndex),
    elementToMove,
    ...arrayWithoutElement.slice(destinationIndex, arrayWithoutElement.length),
  ]
  return newArray
}

export { moveElement, moveElementBetweenArrays }
