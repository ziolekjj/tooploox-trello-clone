import { moveElement, moveElementBetweenArrays } from './move'

test('moveElement inside array', () => {
  const array = [1, 2, 3, 4, 5]
  const sourceIndex = 0
  const destinationIndex = 3
  const expectedArray = [2, 3, 4, 1, 5]
  expect(moveElement({ array, sourceIndex, destinationIndex })).toEqual(expectedArray)
})

test('moveElement inside array edge case when not moving element', () => {
  const array = [1, 2, 3, 4, 5]
  const sourceIndex = 0
  const destinationIndex = 0
  expect(moveElement({ array, sourceIndex, destinationIndex })).toEqual(array)
})

test('moveElement inside array edge case when not moving to the last position', () => {
  const array = [1, 2, 3, 4, 5]
  const sourceIndex = 0
  const destinationIndex = 4
  const expectedArray = [2, 3, 4, 5, 1]
  expect(moveElement({ array, sourceIndex, destinationIndex })).toEqual(expectedArray)
})

test('moveElement between arrays', () => {
  const sourceArray = [1, 2, 3, 4, 5]
  const destinationArray = ['a', 'b', 'c']
  const sourceIndex = 0
  const destinationIndex = 2
  const expectedSourceArray = [2, 3, 4, 5]
  const expectedDestinationArray = ['a', 'b', 1, 'c']
  const { newSourceArray, newDestinationArray } = moveElementBetweenArrays({ sourceArray, destinationArray, sourceIndex, destinationIndex })
  expect(newSourceArray).toEqual(expectedSourceArray)
  expect(newDestinationArray).toEqual(expectedDestinationArray)
})

test('moveElement between arrays when moving to the last position', () => {
  const sourceArray = [1, 2, 3, 4, 5]
  const destinationArray = ['a', 'b', 'c']
  const sourceIndex = 0
  const destinationIndex = 3
  const expectedSourceArray = [2, 3, 4, 5]
  const expectedDestinationArray = ['a', 'b', 'c', 1]
  const { newSourceArray, newDestinationArray } = moveElementBetweenArrays({ sourceArray, destinationArray, sourceIndex, destinationIndex })
  expect(newSourceArray).toEqual(expectedSourceArray)
  expect(newDestinationArray).toEqual(expectedDestinationArray)
})
